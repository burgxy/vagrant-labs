Ansilbe on LXC Boxes..

based on the history of an Ansilbe tutorial

# To create a lxc container named bashy   

$ sudo lxc-create -t ubuntu -n bashy  


#To list all containers   

sudo lxc-ls --fancy  

#To start a container named bashy but detach from it.

sudo lxc-start -n bashy -d

at this time, you can rerun the previous list command to list all containers both running and not.
