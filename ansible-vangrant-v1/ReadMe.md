# Introduction

In accordance to a course on Udemy.com, certification on Ansible, Puppet and Salt Stack. See link below :-

https://www.udemy.com/learning-path-automation-with-ansible-puppet-and-salt/learn/v4/content

I need to set up a little lab environment to run a couple of VMs in other to practice Ansible Automation.  

Since its a 3 in 1 course, and it's starting with the fundementals of Ansible before Puppet then Salt Stack, this folder would be a dedicated to running and understanding the Ansible aspect of the course.   



One thing I need to point out is, in other to follow the course and practice as the tutor demonstrates in the videos, I have adopted using Vagrant VM's (Virtual Machines) as opposed the the LXC containers the tutor used for his demonstration.       


# Why Vagrant and not LXC as the tutor demonstrates ??    

My main reason for adopting Vagrant over LXC containers is for some reasons I cant seem to accurately create LXC boxes on my host machine and troubleshooting that aspect had become so time consuming and I would rather get on with the course by finding alternative methods to create a suitable environment.  therefore I opted for Vagrant.   

# Therefore my aim is ...   

To create the appropriate VMs (Virtual Machines) in a suitable private network to facilitate my udemy certification course, I have reviewed quite a number of online Vagrant tutorials, youtube vidoes as well as other github accounts relating to tutorials that would ease the transition.  Links would be provided at the end of this writeup.


Now first, I have to  install and ensure Vagrant is up and running on my machine.

and this link provides me with a good guidline of what to do.

https://manski.net/2016/09/vagrant-multi-machine-tutorial/  

all history of the commands for the manipulation of Vagrant that I have done for this project would be documentedd and explained in detail in a seperate file called :-   

" commands4vagrant.md " (lol).    

should you wish to follow along, be my guest.

basically, I intend to follow the udemy tutorial but interprete the LXC commands into Vagrant. Hopefully, archieve the same purpose.

  I just found a quikie tutorial on Vangrant and Ansible, check out the link below : -    

  https://knpuniversity.com/screencast/ansible/vagrant-ansible  




# Links to further articles, githubs and vidoes that I have reviewed so far are   


https://www.codereviewvideos.com/course/vagrant-with-ansible-and-symfony2/video/beginners-guide-to-vagrant


http://www.thisprogrammingthing.com/2015/multiple-vagrant-vms-in-one-vagrantfile/  


  https://manski.net/2016/09/vagrant-multi-machine-tutorial/




https://github.com/patrickdlee/vagrant-examples


https://github.com/jningtho/vagrant-poc
