# to confirm that vagrant is installed and know what version you've got

$ vagrant -v

now I have to pull or initialise a box by which would would with.   

Note that they are tons of boxes or vagrant images already stored up at the Vagrant cloud.   and you can find list of boxes I can pull.


  https://www.vagrantup.com/

I can just choose any to my liking and work with that. for this course, I would either be working with precise64 or trusty64 (both are versions of ubuntu) therefore a simple straight forward manipulation.  


# The next is to initialise a box ..

I would go with precise64 for now, and the code goes as such.

$ vagrant init hashicorp/precise64

The above command creates a vagrantfile in a folder maping it to a vagrant box hashicorp/precise64 from the vagrant repository.


# Then

$ vagrant up  

when the the above command is run, vagrant would initially check if the desired box is already within the host machine, i the absense of a previous box, it would download one from vagrantup.com repository.

# To jump into the box

$ vagrant ssh

#OK . This part needs little explanation. its just a working Ubuntu Linux box

the Usuall username when logged into the box is vagrant ( no prices for guessing) LOL

# To jump out of the box

$ ctrl + d

that automatically logs out of the box and back on the host again

# To review the status of the boxes

  $  vagrant status

# Edited the vagrantfile and wish to reload it to take effect

$ vagrant reload



I initially checked for on what I IP was the vagrant box running on, since the ifconfig command is unavailable in this version of the box. I used the old "ip address show" which showed me that the current ip was running on a different ip range as well as a different subnet, other than my host machine. and I tried pinging it from the host and all the packets were lost with no response ..     


I returned to the vagrantfile, and inserted the line below, to archieve a private network. and a designated ip.

config.vm.network "private_network", ip: "192.168.33.10"  

on reloading the vagrant file and having ssh acces to the machine, I have discovered that I have gotten an additional netwok interface as well as the deisired ip.  An IP address that I can ping.

Yay !!! Lets proceed ..

# Now copy and run a script from the course tutorial

 At this time, we need to import a script that would install nginx, set up the configuration files and copy vital files to there appropriate environment or location.   

The cool thing I found out about Vangrant is that any file you store in the same directory as the Vangrantfile (at the time you run vagrant up) is actually assesible from all Vangrant Machines in the /vagrant folder.  There is no need to run scp to copy scripts across VMs as you just have to go to the /vagrant folder and copy from there.


  /vagrant is a shared folder   

I have zipped (tar) a copy or portion of the directory of the scripts required for this part of the tutorial, and I tarred it as jj.tar.gz   


once copied to the VM, unzip the file (jj.tar.gz)

but first, the root account is required to run the script, therefore I have to change to root.

$ sudo -i

Now unzipp the file

$# tar zxvf jj.tar.gz    

the script to run is the install-nginx.sh file

$# ./install-nginx.sh

Now when that script run for me, the nginx service for some reason would not start. the error message I get is ...

Restarting nginx: nginx: [emerg] "worker_processes" directive invalid number in /etc/nginx/nginx.conf:2
nginx: configuration file /etc/nginx/nginx.conf test failed


 How I cirmcumvented that is to edit the 2nd line, in the nginx.conf file and changed the value of auto to 2. I believe its a temporary fix. just to get the service up and running.

 we could verify all services running and port numbers listed when I run the netstat command..   

 $# netstat -tupln

 As well as trying out the ip address on a web browser to comfirm that the installation scripts works.

The nginx server was succesfully installed by the script. 
  
  
  
# Setup Ansible on the node called Ansibley 
  
  
OK , I have established that I can run a script to install nginx. my next plot is to do the same but orchestrating the task wtih Ansible. 
  
  
A quick reference at my vagrantfile you can see I setup a master node called Ansibley along with two other nodes called node1 and node2 respectively. 
  
  
Whilst I am in the Ansibley node, ie ..  
  
$ vagrant ssh ansibley  
  

I am going to install the lastest version Ansible in the machine, (thats were I intend to run ansible from )  
  
$ sudo pip install cryptography --upgrade  
  
$ sudo apt-get install python-pip  
  
$ sudo pip install ansible --upgrade 

If ansible has been installed properly, we could verify that we've got the lastest version of Ansible , (Hopefully version 2.4, as at November 2017)

$ ansible --versions

  
  
Once confirmed that ansible is installed and we got a version, the next step it to create ssh public keys that would be shared across the prospective nodes.   

  

To achieve this, I would use the ssh-keygen app: 

$ ssh-keygen -t rsa  

  
  
a full documentation of how to generate an appropriate public keys can be found at this link.  

 https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2    
 
   
   
If the documentation provided by the link above had been followed correctly, then we should have succesfully created a public and private respectively in the ~/.ssh/ directory in the master node (ansibley)..    

  
  
so, we should have two files that looks like id_rsa and id_rsa.pub. the latter is suffixed .pub to reflect the indication that it is the public key. ( The key to be shared to any host/machine should you wish to gain ssh passwordless entry to the host/machine.)  


# An SSH agreement between two hosts/machines   
  
  
For a user to gain access from one host/machine to the other, they must be some form of understanding within the parties involved.   

  
  
The first time a remote machine is accessed, the remote/destination machine would probe itself for a file located and called ~/.ssh/known_hosts   
  
the remote hosts would (obviously)notice that the machine from which the request is made, is not listed in the ~/.ssh/known_hosts file, and would request permission to store the id of the originating host in the known_hosts file. At this point, you have to explicit type 'YES' (in lowercase) and then hencefort the host making an ssh request is known to the destination host. 

to achieve this, we try a simple ssh login.  

$ ssh vagrant@192.168.33.11     

the at the prompt we type 'yes'   

this would lead to the prompt for the password of the user account we intend to get access into the machine with. In some cases it could be the root account, in other cases it would be some other local account.    

in this case, I am using the default vagrant user account as stated in the ssh login.
  
  
  
I am going to repeat the same steps for the second node (node2).  
  
   
$ ssh vagrant@192.168.33.12     
  
  
Then type 'yes' to store the id of the host in the known_hosts file. 
   
   
Now each 

    

   
   
   


